use array_macro::array;
use nih_plug::prelude::*;
#[macro_use]
extern crate num_derive;
//extern crate num_traits;
use num::{FromPrimitive, ToPrimitive};
use num_traits::Float;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use stoej_dsp::{
    iir_filters::{
        coeffs::ZPK,
        design::{bessel, butter, FilterTopology},
        processors::CascadedBiquad,
    },
    prelude::*,
    processors::MonoProcessor,
};
use tuning::BLOCK_SIZE;

mod tuning;

/// HACK: I could do it more elegantly, but who cares?
static FILTER_CHANGED: AtomicBool = AtomicBool::new(true);

// This is a shortened version of the gain example with most comments removed, check out
// https://github.com/robbert-vdh/nih-plug/blob/master/plugins/examples/gain/src/lib.rs to get
// started

type StereoFilter = ProcParallel<CascadedBiquad<16>, CascadedBiquad<16>>;

struct StoejSciFilters {
    params: Arc<StoejSciFiltersParams>,
    filter: StereoFilter,
    gain: ProcGain<2>,
    sr: f64,
}

#[derive(PartialEq, FromPrimitive, ToPrimitive)]
enum FilterModels {
    Bessel = 0,
    Butterworth = 1,
    Elliptic = 2,
    Chebyshev1 = 3,
    Chebyshev2 = 4,
    OptimumL = 5,
    OnePoleCascade = 6,
}

impl Enum for FilterModels {
    fn variants() -> &'static [&'static str] {
        &[
            "Bessel",
            "Butterworth",
            "Elliptic",
            "Chebyshev 1",
            "Chebyshev 2",
            "Optimum L",
            "One pole cascade",
        ]
    }

    fn ids() -> Option<&'static [&'static str]> {
        None
    }

    fn to_index(self) -> usize {
        self.to_usize().expect("infallible")
    }

    fn from_index(index: usize) -> Self {
        Self::from_usize(index).unwrap()
    }
}

#[derive(PartialEq, FromPrimitive, ToPrimitive)]
enum FilterTypes {
    Bypass = 0,
    LowPass = 1,
    HighPass = 2,
    BandPass = 3,
    BandStop = 4,
}

impl Enum for FilterTypes {
    fn variants() -> &'static [&'static str] {
        &["Bypass", "Low-pass", "High-pass", "Band-pass", "Band-stop"]
    }

    fn ids() -> Option<&'static [&'static str]> {
        None
    }

    fn to_index(self) -> usize {
        self.to_usize().expect("infallible")
    }

    fn from_index(index: usize) -> Self {
        Self::from_usize(index).unwrap()
    }
}

#[derive(Params)]
struct StoejSciFiltersParams {
    /// The parameter's ID is used to identify the parameter in the wrappred plugin API. As long as
    /// these IDs remain constant, you can rename and reorder these fields as you wish. The
    /// parameters are exposed to the host in the same order they were defined. In this case, this
    /// gain parameter is stored as linear gain while the values are displayed in decibels.
    #[id = "gain"]
    pub gain: FloatParam,

    #[id = "filter_model"]
    pub filter_model: EnumParam<FilterModels>,

    #[id = "filter_type"]
    pub filter_type: EnumParam<FilterTypes>,

    #[id = "num_poles"]
    pub num_poles: IntParam,

    #[id = "cutoff"]
    pub cutoff: FloatParam,

    #[id = "bandwidth"]
    pub bandwidth: FloatParam,

    #[id = "pass_band_ripple"]
    pub pass_band_ripple: FloatParam,

    #[id = "stop_band_ripple"]
    pub stop_band_ripple: FloatParam,

    #[id = "pole_spread"]
    pub pole_spread: FloatParam,
}

impl Default for StoejSciFilters {
    fn default() -> Self {
        Self {
            params: Arc::new(StoejSciFiltersParams::default()),
            filter: ProcParallel(CascadedBiquad::unity(), CascadedBiquad::unity()),
            gain: ProcGain { gain: 1. },
            sr: 48_000.0,
        }
    }
}

impl Default for StoejSciFiltersParams {
    fn default() -> Self {
        Self {
            // This gain is stored as linear gain. NIH-plug comes with useful conversion functions
            // to treat these kinds of parameters as if we were dealing with decibels. Storing this
            // as decibels is easier to work with, but requires a conversion for every sample.
            gain: FloatParam::new(
                "Gain",
                util::db_to_gain(0.0),
                FloatRange::Skewed {
                    min: util::db_to_gain(-30.0),
                    max: util::db_to_gain(30.0),
                    // This makes the range appear as if it was linear when displaying the values as
                    // decibels
                    factor: FloatRange::gain_skew_factor(-30.0, 30.0),
                },
            )
            // Because the gain parameter is stored as linear gain instead of storing the value as
            // decibels, we need logarithmic smoothing
            .with_smoother(SmoothingStyle::Logarithmic(50.0))
            .with_unit(" dB")
            // There are many predefined formatters we can use here. If the gain was stored as
            // decibels instead of as a linear gain value, we could have also used the
            // `.with_step_size(0.1)` function to get internal rounding.
            .with_value_to_string(formatters::v2s_f32_gain_to_db(2))
            .with_string_to_value(formatters::s2v_f32_gain_to_db()),

            filter_model: EnumParam::new("Filter model", FilterModels::Butterworth)
                .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            filter_type: EnumParam::new("Filter type", FilterTypes::LowPass)
                .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            num_poles: IntParam::new("Poles", 2, IntRange::Linear { min: 1, max: 16 })
                .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            cutoff: FloatParam::new(
                "Cutoff",
                1000.0,
                FloatRange::Skewed {
                    min: 20.0,
                    max: 20_000.0,
                    factor: 0.5,
                },
            )
            .with_value_to_string(formatters::v2s_f32_hz_then_khz(5))
            .with_string_to_value(formatters::s2v_f32_hz_then_khz())
            .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            bandwidth: FloatParam::new(
                "Bandwidth",
                0.0,
                FloatRange::Skewed {
                    min: 0.0,
                    max: 10_000.0,
                    factor: 0.5,
                },
            )
            .with_value_to_string(formatters::v2s_f32_hz_then_khz(5))
            .with_string_to_value(formatters::s2v_f32_hz_then_khz())
            .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            pass_band_ripple: FloatParam::new(
                "(Pass-band) ripple",
                util::db_to_gain(3.0),
                FloatRange::Skewed {
                    min: util::db_to_gain(0.0),
                    max: util::db_to_gain(30.0),
                    // This makes the range appear as if it was linear when displaying the values as
                    // decibels
                    factor: FloatRange::gain_skew_factor(0.0, 30.0),
                },
            )
            .with_unit(" dB")
            .with_value_to_string(formatters::v2s_f32_gain_to_db(2))
            .with_string_to_value(formatters::s2v_f32_gain_to_db())
            .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            stop_band_ripple: FloatParam::new(
                "Stop-band ripple",
                util::db_to_gain(-48.0),
                FloatRange::Skewed {
                    min: util::db_to_gain(-90.0),
                    max: util::db_to_gain(0.0),
                    // This makes the range appear as if it was linear when displaying the values as
                    // decibels
                    factor: FloatRange::gain_skew_factor(-90.0, 0.0),
                },
            )
            .with_unit(" dB")
            .with_value_to_string(formatters::v2s_f32_gain_to_db(2))
            .with_string_to_value(formatters::s2v_f32_gain_to_db())
            .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),

            pole_spread: FloatParam::new(
                "Pole spread",
                0.0,
                FloatRange::Skewed {
                    min: 0.0,
                    max: 10_000.0,
                    factor: 0.5,
                },
            )
            .with_value_to_string(formatters::v2s_f32_hz_then_khz(5))
            .with_string_to_value(formatters::s2v_f32_hz_then_khz())
            .with_callback(Arc::new(|_| FILTER_CHANGED.store(true, Ordering::Relaxed))),
        }
    }
}

impl Plugin for StoejSciFilters {
    const NAME: &'static str = "STOEJ Sci Filters";
    const VENDOR: &'static str = "Stoejmaskiner";
    const URL: &'static str = env!("CARGO_PKG_HOMEPAGE");
    const EMAIL: &'static str = "panierilorenzo@gmail.com";

    const VERSION: &'static str = env!("CARGO_PKG_VERSION");

    // The first audio IO layout is used as the default. The other layouts may be selected either
    // explicitly or automatically by the host or the user depending on the plugin API/backend.
    const AUDIO_IO_LAYOUTS: &'static [AudioIOLayout] = &[AudioIOLayout {
        main_input_channels: NonZeroU32::new(2),
        main_output_channels: NonZeroU32::new(2),

        aux_input_ports: &[],
        aux_output_ports: &[],

        // Individual ports and the layout as a whole can be named here. By default these names
        // are generated as needed. This layout will be called 'Stereo', while a layout with
        // only one input and output channel would be called 'Mono'.
        names: PortNames::const_default(),
    }];

    const MIDI_INPUT: MidiConfig = MidiConfig::None;
    const MIDI_OUTPUT: MidiConfig = MidiConfig::None;

    const SAMPLE_ACCURATE_AUTOMATION: bool = true;

    const HARD_REALTIME_ONLY: bool = false;

    // If the plugin can send or receive SysEx messages, it can define a type to wrap around those
    // messages here. The type implements the `SysExMessage` trait, which allows conversion to and
    // from plain byte buffers.
    type SysExMessage = ();
    // More advanced plugins can use this to run expensive background tasks. See the field's
    // documentation for more information. `()` means that the plugin does not have any background
    // tasks.
    type BackgroundTask = ();

    fn params(&self) -> Arc<dyn Params> {
        self.params.clone()
    }

    fn initialize(
        &mut self,
        _audio_io_layout: &AudioIOLayout,
        buffer_config: &BufferConfig,
        _context: &mut impl InitContext<Self>,
    ) -> bool {
        // Resize buffers and perform other potentially expensive initialization operations here.
        // The `reset()` function is always called right after this function. You can remove this
        // function if you do not need it.
        self.sr = buffer_config.sample_rate as f64;
        true
    }

    fn reset(&mut self) {
        // Reset buffers and envelopes here. This can be called from the audio thread and may not
        // allocate. You can remove this function if you do not need it.
    }

    fn process(
        &mut self,
        buffer: &mut Buffer,
        _aux: &mut AuxiliaryBuffers,
        _context: &mut impl ProcessContext<Self>,
    ) -> ProcessStatus {
        // expensive updates
        if FILTER_CHANGED.load(Ordering::Relaxed) {
            if self.params.filter_type.value() == FilterTypes::Bypass {
                self.filter.0.set_coeffs_to_unity();
                self.filter.1.set_coeffs_to_unity();
            } else {
                let (topology, critical_freqs) = match self.params.filter_type.value() {
                    FilterTypes::LowPass => (
                        FilterTopology::LowPass,
                        (self.params.cutoff.value() as f64, 0.1),
                    ),
                    FilterTypes::HighPass => (
                        FilterTopology::HighPass,
                        (self.params.cutoff.value() as f64, 0.1),
                    ),
                    FilterTypes::BandPass => (
                        FilterTopology::BandPass,
                        (
                            self.params.cutoff.value() as f64,
                            self.params.bandwidth.value() as f64,
                        ),
                    ),
                    FilterTypes::BandStop => (
                        FilterTopology::BandStop,
                        (
                            self.params.cutoff.value() as f64,
                            self.params.bandwidth.value() as f64,
                        ),
                    ),
                    _ => unreachable!(),
                };

                let order = self.params.num_poles.value();

                let zpk = match self.params.filter_model.value() {
                    FilterModels::Bessel => bessel(order as u32, critical_freqs, topology, self.sr),
                    _ => butter(order, critical_freqs, topology, self.sr),
                }
                .unwrap();

                self.filter.0.set_coeffs_from_zpk(zpk.clone()).unwrap();
                self.filter.1.set_coeffs_from_zpk(zpk).unwrap();
            }

            self.filter.0.reset();
            self.filter.1.reset();
            FILTER_CHANGED.store(false, Ordering::Relaxed);
        }

        // process
        for (_offset, mut block) in buffer.iter_blocks(BLOCK_SIZE) {
            // set gain, skip rest of block
            self.gain.gain = self.params.gain.smoothed.next();
            for _ in 1..BLOCK_SIZE {
                self.params.gain.smoothed.next();
            }

            // process filters
            self.filter
                .0
                .process_replacing_mono(block.get_mut(0).expect("infallible"));
            self.filter
                .1
                .process_replacing_mono(block.get_mut(1).expect("infallible"));

            self.gain
                .process_replacing(&mut block.iter_mut().collect::<Vec<_>>())
        }

        ProcessStatus::Normal
    }

    fn task_executor(&mut self) -> TaskExecutor<Self> {
        // In the default implementation we can simply ignore the value
        Box::new(|_| ())
    }

    fn editor(&mut self, async_executor: AsyncExecutor<Self>) -> Option<Box<dyn Editor>> {
        None
    }

    fn filter_state(state: &mut PluginState) {}

    fn deactivate(&mut self) {}
}

impl ClapPlugin for StoejSciFilters {
    const CLAP_ID: &'static str = "com.stoejmaskiner.sci-filters";
    const CLAP_DESCRIPTION: Option<&'static str> =
        Some("Scientific IIR filters, such as Butterworth");
    const CLAP_MANUAL_URL: Option<&'static str> = Some(Self::URL);
    const CLAP_SUPPORT_URL: Option<&'static str> = None;

    // Don't forget to change these features
    const CLAP_FEATURES: &'static [ClapFeature] = &[ClapFeature::AudioEffect, ClapFeature::Stereo];

    //const CLAP_POLY_MODULATION_CONFIG: Option<PolyModulationConfig> = None;
}

impl Vst3Plugin for StoejSciFilters {
    const VST3_CLASS_ID: [u8; 16] = *b"Exactly16Chars!!";

    // And also don't forget to change these categories
    const VST3_SUBCATEGORIES: &'static [Vst3SubCategory] =
        &[Vst3SubCategory::Fx, Vst3SubCategory::Dynamics];
}

nih_export_clap!(StoejSciFilters);
nih_export_vst3!(StoejSciFilters);
