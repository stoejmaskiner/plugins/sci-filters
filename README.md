# STOEJ Sci Filters

## Building

After installing [Rust](https://rustup.rs/), you can compile STOEJ Sci Filters as follows:

```shell
cargo xtask bundle stoej_sci_filters --release
```
