//! Contains baked-in configurations, to be hand-tuned either for performance or
//! audio quality.

/// the max size of buffer that is processed at a time. Smaller size means more buffers
/// will fit in cache, larger size means more parallelization can occur. This has a
/// large and unpredictable effect on performance, and should be tuned every time the
/// processing chain is altered. Powers of 2 probably work best.
pub const BLOCK_SIZE: usize = 64;
